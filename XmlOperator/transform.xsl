﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xsl:template match="/">
    <xsl:for-each select="/xs:schema/xs:complexType">
      <xsl:text>[Serializable]</xsl:text>
      <xsl:text>&#032;public class </xsl:text>
      <xsl:call-template name="PascalCase">
        <xsl:with-param name="text">
          <xsl:value-of select="@name"/>
        </xsl:with-param>
      </xsl:call-template>
{
<xsl:for-each select="xs:sequence/xs:element | xs:sequence/xs:choice/xs:element">
&#160;&#160;&#160;&#160;<xsl:call-template name="PascalCase">
<xsl:with-param name="text">
<xsl:value-of select="substring-after(@type, 'xs:')"/>
</xsl:with-param>
</xsl:call-template>&#160;<xsl:call-template name="PascalCase">
<xsl:with-param name="text">
<xsl:value-of select="@name"/>
</xsl:with-param>
</xsl:call-template> { get; set; }
</xsl:for-each>}
</xsl:for-each>
</xsl:template>

  <xsl:output omit-xml-declaration="yes" method="text"/>

  <xsl:template name="PascalCase">
    <xsl:param name="text"/>
    <xsl:value-of select="translate(substring($text,1,1),'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')" />
    <xsl:value-of select="translate(substring($text,2,string-length($text)-1),'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')" />
  </xsl:template>
</xsl:stylesheet>