﻿using System;
using System.IO;

namespace XmlOperator
{
	class Program
	{
		static readonly string documentPath = "../../../document1.xml";
		static readonly string schemaPath = "../../../schema.xsd";
		static readonly string transformPath = "../../../transform.xsl";
		static readonly string outTransformPath = "../../../_Generated.cs";

		static void Main(string[] args)
		{
			Program.Validation();
			Program.XPathThings();
			Program.XSLTThings();
			Console.ReadKey();
		}
		
		static void Validation()
		{
			Console.WriteLine("---VALIDATION---");
			Validator validator = new Validator(documentPath, schemaPath);
			var ok = validator.Validate();
			if (ok)
			{
				Console.WriteLine("VALIDATION COMPLETED");
			}
			else
			{
				Console.WriteLine("VALIDATION FAILED");
			}
			Console.WriteLine("---END---");
			Console.WriteLine();
		}

		static void XPathThings()
		{
			Console.WriteLine("---XPATH---");
			XPathSolver solver = new XPathSolver(documentPath);
			int qtCount = 0;
			int sleshCount = 0;
			void x(string xpath)
			{
				qtCount++;
				sleshCount += xpath.Split('/').Length - 1;
				Console.WriteLine($"#{qtCount} query={xpath}");
				var (result, error) = solver.Query(xpath);
				if (error != "")
				{
					Console.WriteLine($"XPATH: {error}");
					return;
				}
				Console.WriteLine(result);
			}
			x("/nations/nation/name");
			x("/nations/nation[agression>3]/name");
			x("//name");
			x("//nation/unit[@power>40]/name");
			x("//nation/unit[@power<=40]/name");
			x("//nation/child::*[text()]");
			x("//building/ancestor::*/name");
			x("//unit[@health>300]/parent::*/name");
			x("//attribute::power");
			x("//attribute::power/parent::*/description");
			x("//*[@health]/name");
			x("//unit/name | //building/name");
			x("/nations/nation[name=\"Germany\"]/following::*");
			x("/nations/nation[name=\"Germany\"]/following::nation/name");
			x("/nations/nation[name=\"Britain Empire\"]/following::*");
			x("/nations/nation[name=\"Britain Empire\"]/following::nation/name");
			x("//nation[name!=\"Germany\"]/unit/name");
			x("//nation[name=\"Germany\"]/preceding::nation/unit/name | //nation[name=\"Germany\"]/following::nation/unit/name");
			x("//nation[agression mod 2 = 0]/preceding::*");
			Console.WriteLine($"COUNT={qtCount} AVERAGE_SLASH={(double)sleshCount / qtCount}");
			Console.WriteLine("---END---");
			Console.WriteLine();
		}

		static void XSLTThings()
		{
			Console.WriteLine("---TRANSFORM---");
			Transformator transformator = new Transformator(transformPath);
			var error = transformator.Transform(schemaPath, outTransformPath);
			if (error != "")
			{
				Console.WriteLine($"XSLT: {error}");
			}
			else
			{
				using (StreamReader reader = new StreamReader(outTransformPath))
				{
					Console.WriteLine(reader.ReadToEnd());
				}
			}
			Console.WriteLine("---END---");
			Console.WriteLine();
		}
	}
}
