﻿using System;
using System.Xml.Xsl;

namespace XmlOperator
{
	class Transformator
	{
		private XslCompiledTransform TransDoc { get; set; }

		public Transformator(string xslPath)
		{
			this.TransDoc = new XslCompiledTransform();
			this.TransDoc.Load(xslPath);
		}

		public string Transform(string dataPath, string outPath)
		{
			try
			{
				this.TransDoc.Transform(dataPath, outPath);
			}
			catch (Exception ex)
			{
				return ex.Message;
			}
			return "";
		}
	}
}
