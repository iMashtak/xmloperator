﻿using System;
using System.Xml;
using System.Xml.Schema;
using System.Linq;

namespace XmlOperator
{
	class Validator
	{
		private bool WasError { get;}

		private XmlDocument Document { get; }

		public Validator(string docPath, string schemaPath)
		{
			this.Document = new XmlDocument();
			this.Document.Load(docPath);
			this.Document.Schemas.Add("", schemaPath);
		}

		public bool Validate()
		{
			this.Document.Validate(this.ValidationEventHandler);
			return !this.WasError;
		}

		private void ValidationEventHandler(object sender, ValidationEventArgs e)
		{
			if (e.Severity == XmlSeverityType.Error)
			{
				Console.WriteLine($"Error: {e.Message}");
				this.WasError = true;
			}
			if (e.Severity == XmlSeverityType.Warning)
			{
				Console.WriteLine($"Warning: {e.Message}");
			}
		}

	}
}
