﻿using System;
using System.Text;
using System.Xml;

namespace XmlOperator
{
	class XPathSolver
	{
		private XmlDocument Document { get; }

		public XPathSolver(string documentPath)
		{
			Document = new XmlDocument();
			Document.Load(documentPath);
		}

		public (string result, string error) Query(string xpath)
		{
			XmlNodeList list = null;
			try
			{
				list = this.Document.SelectNodes(xpath);
			}
			catch (Exception ex)
			{
				return ("", ex.Message);
			}
			StringBuilder builder = new StringBuilder();
			foreach (XmlNode item in list)
			{
				builder.AppendLine(item.InnerText);
			}
			return (builder.ToString(), "");
		}
	}
}
